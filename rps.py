import random
import time

print("Version 3.0")
print("Made by Connor Slagowski")
lose = ("The Computer Wins")
wins = ("You win")
lives = 5
score = 0
draw = 0
computer_lives = 7
account_info_list = []


while True:
    print("To begin, fill in the below information")
    username = input("Please enter your username:    ")
    password = input("Please enter your password:    ")
    searchfile = open("accounts.txt", "rt")
    for line in searchfile:
        account_info_list.append(line.strip())
        if username and password in account_info_list:
            time.sleep(0.5)
            print("Loading...")
            time.sleep(0.5)
            print("Loading...")
            time.sleep(0.5)
            print("Loading...")
            time.sleep(0.5)
            print("Access Granted!")
            print("Rock | Paper | Scissors\n",
                  "Which one will you choose?\n")

            print("Live Rules\n",
                 f"You start with {lives} lives\n",
                  "If you win you get an extra life\n",
                  "If you lose, you lose a life\n",
                  "If you draw, the lives stay the same\n",
                  "------------------------------\n",
                  "MAKE SURE NOT TO USE CAPITALS\n",
                  "------------------------------\n",
                  "You may leave the game at any time by typing 'exit' \n",
                    )
            while True:
                rps = input("Rock, Paper, Scissors?   ")

                computer = ("rock", "paper", "scissors")
                computer = random.choice(computer)

                #rock if statement
                if rps == "rock" and computer == "paper":
                    print(f"The computer chose {computer} \n"
                    "\n",
                    f"{lose} \n",
                    "\n",
                    "\n",
                    "-1 life")
                    lives -= 1
                if rps == "rock" and computer == "scissors":
                    print(f"The computer chose {computer} \n"
                    "\n",
                    f"{wins} \n",
                    "\n",
                    "\n",
                    "+1 life")
                    score += 1
                if rps == "paper" and computer == "scissors":
                    print(f"The computer chose {computer} \n"
                    "\n",
                    f"{lose} \n",
                    "\n",
                    "\n",
                    "-1 life")
                    lives -= 1
                if rps == "paper" and computer == "rock":
                    print(f"The computer chose {computer} \n"
                    "\n",
                    f"{wins} \n",
                    "\n",
                    "\n",
                    "+1 life")
                    score += 1
                if rps == "scissors" and computer == "rock":
                    print(f"The computer chose {computer} \n"
                    "\n",
                    f"{lose} \n",
                    "\n",
                    "\n",
                    "-1 life")
                    lives -= 1
                if rps == "scissors" and computer == "paper":
                    print(f"The computer chose {computer} \n"
                    "\n",
                    f"{wins} \n",
                    "\n",
                    "\n",
                    "+1 life")
                    score += 1
                if rps == computer:
                    print(f"The computer chose {computer} \n"
                    "\n",
                    "It's a draw! \n",
                    "\n",
                    "\n")
                    draw += 1

                #system
                if rps == "display lives":
                    print(f"You have {lives} lives remaining")
                if rps == "display score":
                    print(f"You have {score} points")
                if rps == "diaply draws":
                    print(f"You have drawn on {draw} games")

                #lives
                if score > computer_lives:
                    print("You won! \n",
                    f"You won {score} times",
                    f"You drew {draw} times")
                    stop = input("Press enter to exit")
                    exit()
                    
                if(lives < 1):
                    print("You lose! \n",
                    f"Your score:{score} \n",
                    f"Times drawn: {draw} \n")
                    stop = input("Press enter to exit")
                    exit()

                #exit
                if rps == "exit":
                    break

        else:
            print("Your username or password is incorrect")



                
                


