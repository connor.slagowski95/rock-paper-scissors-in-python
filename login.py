print("Version 3.0")

while True:
    username = input("Pick a username:   ")
    password = input("Pick your password:   ")
    password_confirm = input("Confirm your password:   ")

    if password != password_confirm:
        print("Your passwords do not match. Please try again")

    if password == password_confirm:
        print("Your account has been confirmed")
        text_file = open("accounts.txt", "wt")
        text_file.write("\n")
        text_file.write(username)
        text_file.write("\n")
        text_file.write(password)
        text_file.close()
        break